//Phan Hieu Nghia - 1834104

package inhertitance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		
		books[0] = new Book("Franz Kafka","Metamorphosis");
		books[1] = new ElectronicBook("J.K Rowling","Harry Potter",12);
		books[2] = new Book("Sebastian Barry", "A Long Long Way");
		books[3] = new ElectronicBook("Carlo Collodi","Pinocchio",6);
		books[4] = new ElectronicBook("Twenty Thousand Leagues Under the Sea","Jules Verne", 20);
		
		for (int i = 0; i < books.length; i++) {
			System.out.println(books[i]);
		}
	}
}
