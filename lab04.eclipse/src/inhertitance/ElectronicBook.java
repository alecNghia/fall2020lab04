//Phan Hieu Nghia - 1834104
package inhertitance;

public class ElectronicBook extends Book {
	private int NumberBytes;
	
	public ElectronicBook (String author, String title, int NumberBytes) {
		super(title, author);
		this.NumberBytes = NumberBytes;
	}
	
	public String toString() {
		String fromBase = super.toString();
		return fromBase + ", and the bytes of the selected book is: " + this.NumberBytes;
	}
}
