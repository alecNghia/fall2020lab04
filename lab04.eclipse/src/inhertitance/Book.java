//Phan Hieu Nghia - 1834104

package inhertitance;

public class Book {
	protected String title;
	private String author;
	
	public Book (String title, String author) {
		this.title = title;
		this.author = author;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	public String toString() {
		return "The name of the book is: " + this.title + ", and the author is: " + this.author;
	}
}
