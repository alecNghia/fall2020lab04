//Phan Hieu Nghia - 1834104

package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		
		shapes[0] = new Rectangle(5,7);
		shapes[1] = new Rectangle(2,9);
		shapes[2] = new Circle(2.4);
		shapes[3] = new Circle(4.3);
		shapes[4] = new Square(6);
		
		for (int i = 0; i < shapes.length; i++) {
			System.out.println("The perimeter of the shape is: " + shapes[i].getPerimeter());
			System.out.println("The area of the shape is: " + shapes[i].getArea());
		}
	}
}
