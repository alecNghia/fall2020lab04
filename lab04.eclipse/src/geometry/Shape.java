//Phan Hieu Nghia - 1834104

package geometry;

public interface Shape {
	double getArea();
	
	double getPerimeter();
}
